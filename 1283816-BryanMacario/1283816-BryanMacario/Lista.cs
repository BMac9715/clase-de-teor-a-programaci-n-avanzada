﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1283816_BryanMacario
{
    public class Lista
    {
        private int [] elements;
        private int count;
        public int Count { get { return count; } }

        public void Initialize(int size)
        {
            count = 0;
            elements = new int[size];
        }

        public bool Add(int newElement)
        {
            if(count < elements.Length)
            {
                elements[count++] = newElement;
                return true;
            }
            return false;
        }

        public bool InsertLast(int newElement)
        {
            if(count >= elements.Length)
            {
                elements[count] = newElement;
                count++;
                return true;
            }
            return false;
        }

        public bool InsertFirst(int newElement)
        {
            if(count == elements.Length)
            {
                return false;
            }

            for(int i = count; i > 0; i--)
            {
                elements[i] = elements[i - 1];
            }

            count++;
            elements[0] = newElement;
            return true;
        }

        public bool IsEmpty()
        {
            return count == 0;
        }

        public int FirstElement()
        {
            return count > 0 ? elements[0] : 0;
        }

        public int LastElement()
        {
            return count > 0 ? elements[count - 1] : 0;
        }

        public void Empty()
        {
            count = 0;
        }

        public Lista Tail()
        {
            Lista T = new Lista();
            T.Initialize(count - 1);

            for(int i = 1; i < count; i++)
            {
                T.InsertLast(elements[i]);
            }

            return T;
        } 
    }
}