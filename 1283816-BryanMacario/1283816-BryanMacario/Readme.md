#Programación Avanzada - Ejemplo del Uso de MarkDown

##Nombre:
###Bryan Estuardo Macario Coronado

##Perfil de Bitbuket:
###BMac9715

##Notas del primer ciclo e interciclo:

|						Cursos					   | Nota |
| ------------------------------------------------ |:----:|
| Estrategias De Comunicación Linguistica (CFI)    |  91  |
| Estrategas De Razonamiento (CFI)			       |  92  |
| Introducción a la Informatica y Sistemas		   |  93  |
| Introducción a la Programación			       |  81  |
| Matemática I								       |  81  |
| Química I									       |  91  | 
| Matemática II								       |  84  |
| Matemática Discreta I	

					       |  79  |


# August 31st, 2016

Ejercicio # 1
Crear 1 lista de enteros, 1 pila de enteros  y colas de enteros (10 elementos)
Ingresar 10 elementos aleatoriso del 1 al 100 a la lista.
Recorrer la lista, si el elemento es mayor a 50 ingresar a pila, sino a cola.

Ejercicio # 2
Crear una lista de 100 elementos y 3 colas.
Ingresar 100 elementos aleatorios del 1 al 100 a la lista.
Recorrer la lista, si el elemento es par ingresar a cola1, si es primo a cola2 
y si es impar a cola 3.

Ejercicio # 3
Crear un objeto tipo estudiante.
	Nombre
	Carrera
	Ciclo que cursa
	
Crear una lista tipo estudiante.
Crear dos colas de tipo estudiante. (10 elementos)
Si el estudiante asiste a cursos por la mañana insertar en cola1 de lo contrario a cola2