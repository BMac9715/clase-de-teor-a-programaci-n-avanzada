﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1283816_BryanMacario
{
    /// <summary>
    /// This class will represent an athlete  in Río
    /// </summary>

    public class Athlete
    {
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Discipline { get; set; }
        public String LastMedal { get; set; }

        /// <summary>
        /// This method should be used whenever the olimpian 
        /// wins a competicion  in the last round
        /// </summary>
        /// <param name="medal">gold medal/param>

        public void Win(string medal)
        {
            LastMedal = medal;
        }

    }
}
