﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1283816_BryanMacario
{
    public class Stack
    {
        private int[] elements;
        private int count;

        public int Count
        {
            get
            {
                return count;
            }
        }

        public Stack (int size)
        {
            count = 0;
            elements = new int[size];
        }

        public bool Push(int newElement)
        {
            if(count < elements.Length)
            {
                // count++ primero devuelve count y luego le suma 1
                // ++count primero suma 1 y luego retorna el valor.
                elements[count++] = newElement;
                return true;
            }

            return false;
        }

        public int Top()
        {
            return count > 0 ? elements[count - 1] : 0; //Operador ternario

            /* if(count > 0)
            {
                return elements[count - 1];
            }
            return 0; */
        }

        public int Pop()
        {
            return count > 0 ? elements[--count] : 0;

            //if(count > 0)
            //{
            //    return elements[--count];
            //}
            //return 0;
        }

    }
}
