﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1283816_BryanMacario
{
    class Que
    {
        private int[] Elements;
        private int count;

        public int Count { get { return count} }

        public Que (int Size)
        {
            Elements = new int[Size];
            count = 0;
        }

        /// <summary>
        /// Ingresa un elemento a hacer su cola :3
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>

        public bool Queue(int e)
        {
            if(count < Elements.Length)
            {
                Elements[count] = e;
                count++;
                return true;
            }

            return false;
        }

        public int Deque()
        {
            if(count > 0)
            {
                int t = Elements[0];

                for(int i = 0; i < count -1 ; i++)
                {
                    Elements[i] = Elements[i + 1];
                }
                count--;
                return t;
            }

            return 0;
        }

        public int Front()
        {
            return count > 0 ? Elements[0] : 0;
        }

        public void Empty()
        {
            count = 0;
        }

    }
}
