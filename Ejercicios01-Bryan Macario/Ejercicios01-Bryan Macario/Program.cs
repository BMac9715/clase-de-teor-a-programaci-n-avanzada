﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios01_Bryan_Macario
{
    class Program
    {
        static void Main(string[] args)
        {
            int Choose = 0;
            bool testing = true;
            const int ElementosE2 = 100;

            while(testing)
            {
                Console.WriteLine("         Bienvenido");
                Console.WriteLine(" Eliga que opción desea ejecutar:");
                Console.WriteLine(" 1. Ejercicio 1 \n 2. Ejercicio 2 \n 3. Ejercicio 3 \n 4. Salir");

                try
                {
                    Choose = Convert.ToInt32(Console.ReadLine());
                }
                catch(Exception e)
                {
                    Console.WriteLine("ERROR: " + e.Message);
                    Console.WriteLine("Vuelve a intentarlo.");
                    Console.ReadKey();
                    Console.Clear();
                }
                
                switch(Choose)
                {
                    case 1:

                        #region Ejercicio1:

                        Console.Clear();
                        int[] Datos = new int[10];                       
                        Console.WriteLine("\t\t Ejercicio 1 \n");
                        
                        Console.WriteLine("Ingrese 10 números que esten en el rango 1 a 100: ");
                        for (int i = 0; i < 10; i++)
                        {
                            Datos[i] = Convert.ToInt32(Console.ReadLine());                           
                        }

                        int[] ContenedorCola = AgregarColaE1(Datos).ElementsQueue();

                        Console.WriteLine("\n");

                        Console.WriteLine("Elementos en cola: ");
                        for (int j = 0; j < ContenedorCola.Length; j++)
                        {
                            if(ContenedorCola[j] != 0)
                            {
                                Console.WriteLine(" Elemento " + (j+1).ToString() + ": " + ContenedorCola[j].ToString());
                            }                            
                        }

                        Console.WriteLine("\n");

                        int[] ContenedorPila = AgregarPilaE1(Datos).ElementsStack();
                        Console.WriteLine("Elementos en pila: ");
                        for(int x = 0; x < ContenedorPila.Length; x++)
                        {
                            if(ContenedorPila[x] != 0)
                            {
                                Console.WriteLine(" Elemento " + (x+1).ToString() + ": " + ContenedorPila[x].ToString());
                            }                         
                        }

                        Console.WriteLine("\n\n\t\t      ******** FIN ********");
                        Console.ReadKey();
                        Console.Clear();

                        #endregion

                        break;
                    case 2:

                        #region Ejercicio2:

                        Console.WriteLine("\t\t Ejercicio 2 \n");
                        Console.WriteLine(" Presione cualquier letra, para generar 100 números.");
                        Console.ReadKey();

                        Random Aleatorio = new Random();
                        int[] Lista = new int[ElementosE2];
                        Queue[] Colas = new Queue[3];

                        for(int a = 0; a < Colas.Length; a++)
                        {
                            Colas[a] = new Queue(ElementosE2);
                        }

                        String NumerosImpares = string.Empty;
                        String NumerosPares = string.Empty;
                        String NumerosPrimos = string.Empty;
                        
                        // Genera los 100 números necesarios
                        for(int i = 0; i < Lista.Length; i++)
                        {
                            Lista[i] = Aleatorio.Next(0, 100);
                        }

                        #region Funcion:
                        for (int x = 0; x < Lista.Length; x++)
                        {
                           
                            // Numeros primos
                            if(NumeroPrimo(Lista[x]))
                            {
                                Colas[1].AddQueue(Lista[x]);
                            }

                            // Numeros pares e impares
                            if((Lista[x]%2) == 0)
                            {
                                Colas[0].AddQueue(Lista[x]);
                            }
                            else
                            {
                                Colas[2].AddQueue(Lista[x]);
                            }
                        }

                        #endregion

                        for(int k = 0; k < Colas[0].Count; k++)
                        {
                            if(Colas[0].ElementsQueue()[k] != 0)
                            {
                                NumerosPares += Convert.ToString(Colas[0].ElementsQueue()[k]) + ", ";
                            }                            
                        }

                        for(int b = 0; b < Colas[1].Count; b++)
                        {
                            if(Colas[1].ElementsQueue()[b] != 0)
                            {
                                NumerosPrimos += Convert.ToString(Colas[1].ElementsQueue()[b]) + ", ";
                            }
                        }

                        for(int c = 0; c < Colas[2].Count; c++)
                        {
                            if (Colas[2].ElementsQueue()[c] != 0)
                            {
                                NumerosImpares += Convert.ToString(Colas[2].ElementsQueue()[c]) + ", ";
                            }
                        }

                        Console.WriteLine("\n Los números pares son: " + NumerosPares);
                        Console.WriteLine("\n Los números primos son: " + NumerosPrimos);
                        Console.WriteLine("\n Los números impares son: " + NumerosImpares);

                        Console.WriteLine("\n\n\t\t      ******** FIN ********");
                        Console.ReadKey();
                        Console.Clear();

                        #endregion

                        break;
                    case 3:

                        #region Ejercicio3:
                        Console.Clear();

                        bool Eleccion = true;
                        Estudiante[] Estudiantes = new Estudiante[10];
                        QueueStudent EstudiantesMatutina = new QueueStudent(10);
                        QueueStudent EstudiantesVespertina = new QueueStudent(10);

                        for (int i = 0; i < Estudiantes.Length; i++)
                        {
                            Estudiantes[i] = new Estudiante();
                        }

                        #region IngresoDatos:

                        int Total = 0;
                        while (Eleccion)
                        {
                            if( Total <= 10)
                            {
                                Console.WriteLine("Bienvenido al registro de estudiantes. \n");
                                Console.Write(" Ingrese el nombre del estudiante: ");
                                Estudiantes[Total].Nombre = Console.ReadLine();
                                Console.Write("\n Ingrese el nombre de la carrera que estudia: ");
                                Estudiantes[Total].Carrera = Console.ReadLine();
                                Console.Write("\n Ingrese el ciclo que actualmente esta cursando: ");
                                Estudiantes[Total].CicloActual = Convert.ToInt32(Console.ReadLine());
                                Total++;
                                Console.WriteLine("\n ¿Desea agregar otro estudiante?  \n\t SI --> PRESIONE 1 \n\t NO --> PRESIONE CUALQUIER OTRO NUMERO");
                                int Pregunta = Convert.ToInt32(Console.ReadLine());

                                if (Pregunta == 1)
                                {
                                    Console.Clear();
                                }
                                else
                                {
                                    Eleccion = false;
                                }
                            }
                            else
                            {
                                Eleccion = false;
                                Console.WriteLine("\n\t La lista esta llena.");
                                Console.ReadKey();
                            }
                            
                        }
                        #endregion

                        for(int j = 0; j < Estudiantes.Length; j++)
                        {
                            if(Estudiantes[j].CicloActual >= 6)
                            {
                                EstudiantesVespertina.AddQueue(Estudiantes[j]);
                            }
                            else
                            {
                                EstudiantesMatutina.AddQueue(Estudiantes[j]);
                            }
                        }

                        #region Imprimir:

                        Console.WriteLine("\n\n\t\t *********** DATOS ***********");
                        Console.WriteLine("\n\n Estudiantes Vespertina");

                        for (int w = 0; w < EstudiantesVespertina.Count; w++)
                        {
                            if (EstudiantesVespertina.ElementsQueue()[w] != null)
                            {
                                Console.WriteLine("\n Estudiante " + (w + 1).ToString());
                                Console.Write("\n    Nombre: " + EstudiantesVespertina.ElementsQueue()[w].Nombre);
                                Console.Write("  Carrera: " + EstudiantesVespertina.ElementsQueue()[w].Carrera);
                                Console.Write("  Ciclo que cursa: " + EstudiantesVespertina.ElementsQueue()[w].CicloActual.ToString() + "\n");
                            }
                        }

                        Console.WriteLine("\n\n Estudiantes Matutina");

                        for(int v = 0; v < EstudiantesMatutina.Count; v++)
                        {
                            if (EstudiantesMatutina.ElementsQueue()[v].Nombre != null)
                            {
                                Console.WriteLine("\n Estudiante " + (v + 1).ToString());
                                Console.Write("\n    Nombre: " + EstudiantesMatutina.ElementsQueue()[v].Nombre);
                                Console.Write("  Carrera: " + EstudiantesMatutina.ElementsQueue()[v].Carrera);
                                Console.Write("  Ciclo que cursa: " + EstudiantesMatutina.ElementsQueue()[v].CicloActual.ToString() + "\n");
                            }
                        }
                                            
                        #endregion

                        Console.WriteLine("\n\n\t\t      ******** FIN ********");
                        Console.ReadKey();
                        Console.Clear();

                        #endregion

                        break;
                    case 4:
                        Environment.Exit(0);
                        break;
                }
            }

            Console.ReadKey();
        }

        static Queue AgregarColaE1 (int[] lista)
        {
            Queue ColaE1 = new Queue(lista.Length);
           
            for (int i = 0; i < lista.Length; i++)
            {
                if (lista[i] < 50)
                {
                    ColaE1.AddQueue(lista[i]);
                }
            }

            return ColaE1;
        }

        static Stack AgregarPilaE1(int[] lista)
        {
            Stack PilaE1 = new Stack(lista.Length);

            for (int i = 0; i < lista.Length; i++)
            {
                if (lista[i] >= 50)
                {
                    PilaE1.Push(lista[i]);
                }
            }
            return PilaE1;
        }

        static bool NumeroPrimo(int number)
        {
            int count = 0;

            for (int z = 1; z < (number + 1); z++)
            {
                if ((number % z) == 0)
                {
                    count++;
                }
            }

            // Numeros primos
            if (count != 2)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}
