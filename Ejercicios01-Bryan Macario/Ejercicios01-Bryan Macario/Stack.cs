﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios01_Bryan_Macario
{
    class Stack
    {
        private int[] elements;
        private int count;

        public int Count
        {
            get
            {
                return count;
            }
        }

        public Stack(int size)
        {
            count = 0;
            elements = new int[size];
        }

        public bool Push(int newElement)
        {
            if (count < elements.Length)
            {
                elements[count++] = newElement;
                return true;
            }

            return false;
        }

        public int Top()
        {
            return count > 0 ? elements[count - 1] : 0; //Operador ternario
        }

        public int Pop()
        {
            return count > 0 ? elements[--count] : 0;
        }

        public int[] ElementsStack()
        {
            return elements;
        }

    }
}
