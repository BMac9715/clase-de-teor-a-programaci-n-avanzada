﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios01_Bryan_Macario
{
    class QueueStudent
    {

        private Estudiante[] Elements;
        private int count;

        public int Count { get { return count; } }

        public QueueStudent(int Size)
        {
            Elements = new Estudiante[Size];
            count = 0;
        }

        /// <summary>
        /// Ingresa un elemento a hacer su cola :3
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>

        public bool AddQueue(Estudiante NewStudent)
        {
            if (count < Elements.Length)
            {
                Elements[count] = NewStudent;
                count++;
                return true;
            }

            return false;
        }

        public bool Deque()
        {
            if (count > 0)
            {
                for (int i = 0; i < count - 1; i++)
                {
                    Elements[i] = Elements[i + 1];
                }
                count--;
                return true;
            }

            return false;
        }

        public Estudiante Front()
        {
            return count > 0 ? Elements[0] : null;
        }

        public void Empty()
        {
            count = 0;
        }

        public Estudiante[] ElementsQueue()
        {
            return Elements;
        }

    }
}
