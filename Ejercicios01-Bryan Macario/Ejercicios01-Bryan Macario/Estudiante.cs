﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios01_Bryan_Macario
{
    public class Estudiante
    {
        private String nombre;
        private String carrera;
        private int cicloActual;

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Carrera
        {
            get
            {
                return carrera;
            }

            set
            {
                carrera = value;
            }
        }

        public int CicloActual
        {
            get
            {
                return cicloActual;
            }

            set
            {
                cicloActual = value;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Estudiante()
        {
        }


    }
}
