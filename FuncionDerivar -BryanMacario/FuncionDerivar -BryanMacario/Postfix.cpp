#include "stdafx.h"
#include "Postfix.h"
#include <stack>;


CPostfix::CPostfix()
{
}

string CPostfix::PostFix(string E)
{
	stack<char> numeros;
	stack<char> operadores;
	string resultado;

	for (int i = 0; i < E.length(); i++)
	{
		if (!IsOperator(E[i]))
		{
			numeros.push(E[i]);
			resultado += E[i];
		}
		else
		{
			if (operadores.empty())
			{
				operadores.push(E[i]);
			}
			else
			{
				if (Jerarquia(operadores.top()) > Jerarquia(E[i]))
				{
					operadores.push(E[i]);
				}
				else
				{
					while (Jerarquia(operadores.top()) > Jerarquia(E[i]))
					{
						if (E[i] == '+')

							numeros.top();
						    
						if (E[i] == '-')
							numeros.push(-numeros.pop() + numeros.pop());
						if (E[i] == '*')
							numeros.push(numeros.pop() * numeros.pop());
						if (E[i] == '/')
							numeros.push((1 / numeros.pop()) * numeros.pop());
						resultado += E[i];
					}
				}
			}
		}
	}


}

int CPostfix::Jerarquia(char Op)
{
	switch (Op)
	{
	case '~':
		return 1;
		break;

	case '+':
	case '-':
		return 2;
		break;

	case '*':
	case '/':
		return 3;
		break;

	case '^':
		return 4;
		break;

	//Recordatorio: Agregar el parentesis

	default:
		return 1;
		break;
	}
}


bool CPostfix::IsOperator(char c)
{
	switch (c)
	{
	case '^':
		return true;
		break;
	case '*':
		return true;
		break;;
	case '/':
		return true;
		break;
	case '+':
		return true;
		break;
	case '-':
		return true;
		break;
	case '~':
		return true;
		break;
	default:
		return false;
		break;
	}
}

CPostfix::~CPostfix()
{
}
