#pragma once
class DNode
{
public:
	DNode();
	~DNode();

protected:

	int value;
	DNode *next;
	DNode *previous;

public:
	DNode * getPrevious();
	void setPrevious(DNode * aNode);
	void setNext(DNode * aNode);
	DNode * getNext();
	int getValue();
	void setValue(int number);
};



