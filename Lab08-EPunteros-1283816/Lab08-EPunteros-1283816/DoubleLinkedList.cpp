#include "stdafx.h"
#include "DoubleLinkedList.h"


DoubleLinkedList::DoubleLinkedList()
{
	this->head = nullptr;
	this->tail = nullptr;
}


DoubleLinkedList::~DoubleLinkedList()
{
	DNode *iterator = nullptr;
	while (this->head)
	{
		iterator = this->head;
		this->head = this->head->getNext();
		delete iterator;
	}
	this->head = nullptr;
	this->tail = nullptr;
}

int DoubleLinkedList::Elements()
{
	return count;
}


void DoubleLinkedList::Add(int number)
{
	DNode *node = new DNode();
	node->setValue(number);

	if (this->head)
	{
		node->setPrevious(this->tail);
		this->tail->setNext(node);
		this->tail = node;

	}
	else
	{
		this->head = node;
		this->tail = node;
	}

	count++;
}

/*
Elimina el elemento en la posici�n 'index'
*/
void DoubleLinkedList::DeleteAtIndex(int index)
{
	DNode *aux = new DNode();
	DNode *aux1 = new DNode();

	aux = this->head;
	aux1 = aux->getNext();

	if (index < 1 || index > count)
	{
		//Genera un error
	}
	else
	{
		if (index == 1)
		{
			this->head = aux->getNext();
		}
		else
		{
			for (int i = 2; i <= index; i++)
			{
				if (index == i)
				{
					DNode *aux_node = new DNode();
					aux->setNext(aux1->getNext());
					delete aux_node;
					
					if(i == count)
					{
						this->tail = aux;
						this->tail->setPrevious(aux->getPrevious());
					}

					count--;
				}
				else
				{
					aux = aux->getNext();
					aux1 = aux->getNext();
				}			
			}
		}
	}


}

/*
Devuelve una nueva lista con las instancias de "number"
*/
DoubleLinkedList * DoubleLinkedList::GetEveryInstanceOf(int number)
{
	return nullptr;
}

/*
Devuelve una nueva lista ordenada con las instancias de cada elemento a y b. Asuma que a y b, est�n ordenadas.
*/
DoubleLinkedList * DoubleLinkedList::MergeList(DoubleLinkedList * a, DoubleLinkedList * b)
{
	return nullptr;
}

/*
Agrega todos los elementos de la lista a, en la lista actual.
*/
void DoubleLinkedList::CopyElementsFrom(DoubleLinkedList * a)
{
}

/*
Inserta un elemento de manera ordenada
*/
void DoubleLinkedList::InsertOrdered(int number)
{
	DNode *node = new DNode();

	if(count == 0)
	{
		node->setValue((number));
		this->head = node;
		this->tail = node;
		count++;
	}
	else
	{
		DNode *aux = this->head;
		DNode *aux1 = this->head->getNext();
			
		for (int i = 1; i <= count; i++)
		{
			if(number < aux->getValue())
			{
				node->setValue(number);
				this->head = node;
				this->head->setNext(aux);
				this->tail = aux;
				this->tail->setPrevious(this->head);
				
			}
		}

	}


}

/*
Muestra en pantalla todos los elementos de la lista separados por coma
*/
void DoubleLinkedList::Print()
{
}
