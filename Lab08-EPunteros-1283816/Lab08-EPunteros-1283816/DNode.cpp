#include "stdafx.h"
#include "DNode.h"


DNode::DNode()
{
	this->previous = nullptr;
	this->value = 0;
	this->next = nullptr;
}


DNode::~DNode()
{
}


DNode * DNode::getPrevious()
{
	return this->previous;
}


void DNode::setPrevious(DNode * aNode)
{
	this->previous = aNode;
}


void DNode::setNext(DNode * aNode)
{
	this->next = aNode;
}


DNode * DNode::getNext()
{
	return this->next;
}


int DNode::getValue()
{
	return this->value;
}


void DNode::setValue(int number)
{
	this->value = number;
}
