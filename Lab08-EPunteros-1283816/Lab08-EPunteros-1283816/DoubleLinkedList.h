#pragma once
#include "DNode.h"
class DoubleLinkedList
{
public:
	DoubleLinkedList();
	~DoubleLinkedList();
	int count;
protected:
	DNode *head;
	DNode *tail;
public:
	int Elements();
	void Add(int number);
	void DeleteAtIndex(int index); //Terminado
	DoubleLinkedList * GetEveryInstanceOf(int number);
	void CopyElementsFrom(DoubleLinkedList * a);
	void InsertOrdered(int number); //Terminado

	static DoubleLinkedList * MergeList(DoubleLinkedList * a, DoubleLinkedList * b);

	void Print(); // Por terminar
};

