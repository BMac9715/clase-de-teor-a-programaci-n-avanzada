#Programaci�n Avanzada - Ejemplo del Uso de MarkDown

##Nombre:
###Bryan Estuardo Macario Coronado

##Perfil de Bitbuket:
###BMac9715

##Notas del primer ciclo e interciclo:

|						Cursos					   | Nota |
| ------------------------------------------------ |:----:|
| Estrategias De Comunicaci�n Linguistica (CFI)    |  91  |
| Estrategas De Razonamiento (CFI)			       |  92  |
| Introducci�n a la Informatica y Sistemas		   |  93  |
| Introducci�n a la Programaci�n			       |  81  |
| Matem�tica I								       |  81  |
| Qu�mica I									       |  91  | 
| Matem�tica II								       |  84  |
| Matem�tica Discreta I								|  79  |


# August 31st, 2016

Ejercicio # 1
Crear 1 lista, 1 pila y cola (10 elementos)
Ingresar 10 elementos aleatorios del 1 al 100 a la lista.
Recorrer la lista, si el elemento es mayor a 50 ingresar a pila, sino a cola.

Ejercicio # 2
Crear una lista de 100 elementos y 1 colas.
Ingresar 100 elementos aleatorios del 1 al 100 a la lista.
Recorrer la lista, si el elemento es par ingresar a cola1, si es primo a cola2 
y si es impar a cola 3.

Ejercicio # 3
Crear un objeto tipo estudiante.
	Nombre
	Carrera
	Ciclo que cursa
	
Crear una lista tipo estudiante.
Crear dos colas de tipo estudiante. (10 elementos)
Si el estudiante asiste a cursos por la ma�ana insertar en cola1 de lo contrario a cola2

# September 05th, 2016

Implementaci�n de metodos para colas y pilas en lenguaje C#.
Se crearon metodos como:
	Encolar
	Apilar
	Desencolar
	Vacia
	Desapilar
	VerValor

# September 11th, 2016

A partir de la definici�n de recursividad: 
	"Si dentro de un m�todo existe la llamada a s� mismo decimos que el m�todo es recursivo.
	 Cuando un m�todo se llama a s� mismo, se asigna espacio en la pila para las nuevas variables 
	 locales y par�metros."
Se implementar�n ejercicios para demostrar como funciona la recursividad, en la cual
es necesaria tener un caso base, este rompera el ciclo y har� que se resuelva el ciclo..

Ejercicios resueltos:
	Fibonacci
	Numeros Primos
	Factorial

# November 29th, 2016
Se realizo la actualizaci�n al repositorio, por motivos de descuido no se sincronizar�n los
ejercicios en el momento conveniente.
Ademas se agregan los resultados de los examenes parciales que son parte del curso.

En los ejercicios que se agregan resalta el cambio de lengujae de C# a C++ debido 
a que el tema que continuaba era el de punteros.

Tomando como base los punteros se partio hacia los otros temas, como lo son las colas y pilas
dinamicas, listas enlazadas, listas doblemente enlazadas y arboles de expresion.

En la actualizaci�n se implementarion en codigo cada uno de los temas mencionados anteriormente
ademas de un ejercicio de sobrecarga de Operadores realizado en lenguaje C++,

##Examen Parcial 1

Se evaluaron los temas de pilas, colas y sobre todo la recursividad.

##Examen Parcial 2

Se evaluaron los temas de memoria, jerarquia de memoria, listas doblemente enlazadas,
generar el prefijo de una expresi�n del cual se armaba un �rbol de expresiones.


