﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recursividad_ClaseTeoria
{
    public class Recursion
    {

        public int FactoriarRecursivo(int value)
        {
            if(value == 0)
            {
                return 1;
            }
            else
            {
                return FactoriarRecursivo(value - 1) * value;
            }
        }

        public int ModuloInterativo(int x, int y)
        {
            int t = x;

            while(t>y)
            {
                t -= y;
            }
            return t;
        }

        public int ModuloRecursivo(int x, int y)
        {
            if()
            {

            }


        }

        public int FibonacciRecursivo(int value)
        {
            if (value == 1)
            {
                return 0;
            }

            if (value == 2)
            {
                return 1;
            }

            return FibonacciRecursivo(value - 2) + FibonacciRecursivo(value - 1);
        } // Value = al numero hasta donde se desea llegar.

        public String CadenaInvertida(String Cadena)
        {
            if (Cadena.Length == 1)
            {
                return Cadena;
            }
            else
            {
                return Cadena[Cadena.Length - 1] + CadenaInvertida(Cadena.Substring(0, Cadena.Length - 1));
            }
        }

        public int MultiplicacionDeArray(int[] Array, int Tamaño)
        {
            if(Tamaño == 1)
            {
                return Array[0];
            }

            return MultiplicacionDeArray(Array, (Tamaño - 1)) * (Array[Tamaño -1]);
        } //Recibe el arreglo y el tamaño del arreglo

        public int Gxy(int x, int y)
        {
            if (x < y)
            {
                return (2 * x) - y;

            }

            return (1 / Gxy(x - y, x + 1));
        } // Funcion que busca el menor y mayor.. 


    }
}
