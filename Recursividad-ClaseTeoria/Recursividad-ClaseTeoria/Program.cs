﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recursividad_ClaseTeoria
{
    class Program
    {
        static void Main(string[] args)
        {
            Recursion Pruebas = new Recursion();
            int n = 7;
            String cadena = "MACA";
            int[] ArrayPrueba = new int[3];
            ArrayPrueba[0] = 2;
            ArrayPrueba[1] = 3;
            ArrayPrueba[2] = 3;
            int x = 5;
            int y = 3;


            Console.WriteLine("         EJERCICIOS DE RECURSIVIDAD");
            Console.WriteLine("\n  1. Factorial Recursivo");
            Console.WriteLine("     Prueba de factorial con '{0}': {1}", n, Pruebas.FactoriarRecursivo(n));
            Console.WriteLine("\n  2. Fibonacci Recursivo");
            Console.WriteLine("     Prueba de Fibonacci con '{0}': {1}", n, Pruebas.FibonacciRecursivo(n));
            Console.WriteLine("\n  3. Cadena Invetida");
            Console.WriteLine("     Prueba de Fibonacci con '"+ cadena +"' : " + Pruebas.CadenaInvertida(cadena));
            Console.WriteLine("\n  3. Producto de un arreglo");
            Console.WriteLine("     Prueba de producto de un arreglo : {0}", Pruebas.MultiplicacionDeArray(ArrayPrueba, ArrayPrueba.Length ));
            Console.WriteLine("\n  4. Funcion G(xy) ");
            Console.WriteLine("     Prueba de la función con X = {0} y Y = {1}, resultado: {2} ", x, y, Pruebas.Gxy(x, y));

            Console.ReadKey();

        }

    }
}
