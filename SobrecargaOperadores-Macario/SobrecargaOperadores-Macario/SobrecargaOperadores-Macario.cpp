// SobrecargaOperadores-Macario.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <random>
#include <string>
#include <list>
#include <stdlib.h>
#include <time.h>
#include <sstream>
#include "Lista.h"
#include <iostream>

using namespace std;

//Sobrecarga del operador +
Lista& operator+(Lista &listaA, Lista &listaB)
{
	list<int> temp;
	list<int> aux;
	temp = listaA.datos;
	aux = listaB.datos;

	while(!aux.empty())
	{
		temp.push_back(aux.front());
		aux.pop_front();
	}

	return *(new Lista(temp));
}

//Sobrecarga del operador -
Lista& operator-(Lista &listaA, Lista &listaB)
{
	list<int> temp;
	list<int> aux;
	list<int> resultanteA;
	list<int> resultanteB;
	list<int> respuesta;

	int apoyo = 0;

	resultanteA = listaA.datos;
	resultanteB = listaB.datos;
	temp = listaA.datos;
	
	while (!temp.empty())
	{
		aux = listaB.datos;

		while(!aux.empty())
		{
			if(temp.front() == aux.front())
			{
				apoyo = aux.front();
				resultanteA.remove(apoyo);
				resultanteB.remove(apoyo);
			}
			aux.pop_front();
		}

		temp.pop_front();
	}

	respuesta = resultanteA;

	while (!resultanteB.empty())
	{
		respuesta.push_back(resultanteB.front());
		resultanteB.pop_front();
	}

	return *(new Lista(respuesta));
}

int main()
{
	const int ELEMENTOS_LISTA = 10;

	string elementosA = "";
	string elementosB = "";
	string elementosC = ""; //Para sobrecarga operador +
	string elementosD = ""; //Para sobrecarga operador -

	list<int> a; //Lista a
	list<int> b; //Lista b
	list<int> c; //Lista resultante
	list<int> temp; //Ayuda a imprimir los elementos de las listas...

	srand(time(NULL));
	//Llena la lista con n�meros aleatorios...
	for (int i = 0; i < ELEMENTOS_LISTA; i++)
	{
		a.push_back(1 + rand() % (101 - 1));
		b.push_back(1 + rand() % (101 - 1));
	}

	//Instancias de la clase LISTA
	Lista A(a);
	Lista B(b);

	//Para mostrar en pantalla...
	temp = A.datos;
	while(!temp.empty())
	{
		elementosA += to_string(temp.front()) + " ";
		temp.pop_front();
	}

	//Para mostrar e pantalla...
	temp = B.datos;
	while (!temp.empty())
	{
		elementosB += to_string(temp.front()) + " ";
		temp.pop_front();
	}
	
	system("color 06");
	cout << "\n\t\t\t Ejercicios sobre SOBRECARGA" << endl;
	cout << "\nBryan Macario - 1283816" << endl;

	cout << "\n\nElementos de la lista A" <<endl;
	cout << elementosA << endl;
	cout << "\nElementos de la lista B" << endl;
	cout << elementosB << "\n" << endl;

	system("PAUSE");

	//Hace la sobrecarga del operador
	Lista C = A + B;

	temp = C.datos;
	while (!temp.empty())
	{
		elementosC += to_string(temp.front()) + " ";
		temp.pop_front();
	}
	
	cout << "\n\nEjemplo: Sobrecarga del operador +" << endl;
	cout << "\nElementos de la lista C" << endl;
	cout << elementosC << "\n" << endl;

	system("PAUSE");

	//Hace la sobrecarga del operador -
	Lista D = A - B;

	temp = D.datos;
	while (!temp.empty())
	{
		elementosD += to_string(temp.front()) + " ";
		temp.pop_front();
	}

	cout << "\n\nEjemplo: Sobrecarga del operador -" << endl;
	cout << "\nElementos de la lista C" << endl;
	cout << elementosD << "\n" << endl;

	system("\nPAUSE");

    return 0;
}

