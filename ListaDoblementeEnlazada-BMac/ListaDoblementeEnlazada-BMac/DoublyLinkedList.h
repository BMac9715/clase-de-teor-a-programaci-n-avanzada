#pragma once
#include "Node.h"
class DoublyLinkedList
{
public:
	DoublyLinkedList();
	~DoublyLinkedList();

	CNode* head;
	CNode* queue;

	void Add(int newValue);
	bool RemoveFirstElement(int deiValue);
	bool IsEmpty() const;
	int count;
};

